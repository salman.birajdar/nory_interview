from sqlalchemy.orm import aliased

from revenue.app import db
from datetime import datetime
import pandas as pd


# Initializing tables for the DB

class Company(db.Model):
    __tablename__ = 'Company'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, nullable=False)
    tables = db.relationship('Tables', backref='Company', lazy=True)


class Tables(db.Model):
    __tablename__ = 'Tables'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, nullable=True)
    floor_id = db.Column(db.Integer, index=True, nullable=True)
    floor_name = db.Column(db.String(64), index=True, nullable=True)
    orders = db.relationship('Orders', backref='Tables', lazy=True)
    company_id = db.Column(db.Integer, db.ForeignKey('Company.id'), nullable=False)


class Orders(db.Model):
    __tablename__ = 'Orders'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    receipt_id = db.Column(db.String(64), index=True, nullable=True)
    creation_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    modified_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    finalized_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    print_date = db.Column(db.String(64), index=True, nullable=True)
    status = db.Column(db.String(64), index=True, nullable=True)
    type = db.Column(db.String(64), index=True, nullable=True)
    void_status = db.Column(db.String(64), index=True, nullable=True)
    cust_id = db.Column(db.String(64), index=True, nullable=True)
    delivery_date = db.Column(db.String(64), index=True, nullable=True)
    net_total = db.Column(db.Float, index=True, nullable=True)
    tax = db.Column(db.String(64), index=True, nullable=True)
    total = db.Column(db.Float, index=True, nullable=True)
    tip = db.Column(db.Float, index=True, nullable=True)
    user_id = db.Column(db.String(64), index=True, nullable=True)
    seq_num = db.Column(db.String(64), index=True, nullable=True)
    lite_ser_id = db.Column(db.String(64), index=True, nullable=True)
    table_id = db.Column(db.Integer, db.ForeignKey('Tables.id'),
                          nullable=False)

    def __repr__(self):
        return 'Table ID: {} , receipt id: {}'.format(self.id,self.receipt_id)



# Function to load the data from csv into our tables
def load_data():
    data = pd.read_csv("../../data/data.csv", low_memory=False)
    for index1, row in data.iterrows():
        query_res = db.session.query(db.exists().where(Company.id == row['Company ID'])).scalar()
        if not query_res:
            obj_comp = Company(id=row['Company ID'],name=row['Company Name'])
            db.session.add(obj_comp)
            db.session.commit()

        if not db.session.query(db.exists().where(Tables.id == row['Table ID'])).scalar():
            obj_table = Tables(id=row['Table ID'],name= row['Table Name'],floor_id= row['Floor ID'],floor_name= row['Floor Name'],company_id = row['Company ID'])
            db.session.add(obj_table)
            db.session.commit()

        obj_order = Orders(receipt_id= row['Receipt ID'],creation_date= datetime.strptime(row['Creation Date'], '%d/%m/%y %H:%M'),modified_date= datetime.strptime(row['Modified Date'], '%d/%m/%y %H:%M'),
                           finalized_date= datetime.strptime(row['Finalized Date'], '%d/%m/%y %H:%M'),
                           print_date= row['Print Date'] ,status= row['Status'],type= row['Type'],void_status= row['Void Status'],cust_id= row['Customer ID'],
                           delivery_date=row['Delivery Date'],
                           net_total=row['Net Total'],tax= row['Taxes'],total= row['Total'],tip= row['Tip'],user_id= row['User ID'],seq_num= row['Sequence Number'], lite_ser_id=row['Liteserver ID'],table_id=row['Table ID'])
        db.session.add(obj_order)
        db.session.commit()


db.create_all()
load_data()


# class Entry(db.Model):
#     __tablename__ = 'entries'
#
#     """
#     Sample Entry
#     """
