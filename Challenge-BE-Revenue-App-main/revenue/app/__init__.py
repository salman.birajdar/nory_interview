from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy.sql import func, extract


from revenue.app.config import Config

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
db.init_app(app)
migrate = Migrate(app, db)


from revenue.app.models import Orders,Company,Tables


@app.route("/admin", methods=["GET"])
def index():
    order= Orders.query.all()
    return jsonify(hello="Company: {}".format(order))


@app.route("/hourly")
def hourly():
    branch_list = {"2hg8j32gw8g": "Nory Taco", "345hngydkgs": "Nory Pizza", "352h67i328fh": "Nory Sushi"}
    b_id = request.args.get('branch_id')
    start = request.args.get('start')
    end = request.args.get('end')

    # some_engine = create_engine('postgresql://scott:tiger@localhost/')
    # Session = sessionmaker(bind=some_engine)
    # session = Session()


    res = Orders.query\
        .join(Tables, Orders.table_id==Tables.id)\
        .join(Company, Tables.company_id==Company.id)\
        .add_columns(Orders.id, Orders.receipt_id, Tables.company_id, Orders.finalized_date, Company.name,Orders.total)\
        .filter(Company.name == branch_list[b_id], db.func.date(Orders.finalized_date)<=end, db.func.date(Orders.finalized_date)>=start) \
        .group_by(extract('hour',Orders.finalized_date))\
        .with_entities(Orders.finalized_date, db.func.sum(Orders.total).label("sum_statistics"))\
        .all()
    print(res)


    # orders=db.session.query.first()
    # print(orders)
    # for row in rev:
    #     print(row)
    #     return jsonify(msg="implement me - {}".format(row))
    #     break
    # entries = Orders.query.

    # return jsonify(msg="implement me - {}".format(row))
    return str(res)


@app.route('/daily')
def daily():
    branch_list = {"2hg8j32gw8g": "Nory Taco", "345hngydkgs": "Nory Pizza", "352h67i328fh": "Nory Sushi"}
    b_id = request.args.get('branch_id')
    start = request.args.get('start')
    end = request.args.get('end')
    res = Orders.query \
        .join(Tables, Orders.table_id == Tables.id) \
        .join(Company, Tables.company_id == Company.id) \
        .add_columns(Orders.id, Orders.receipt_id, Tables.company_id, Orders.finalized_date, Company.name, Orders.total) \
        .filter(Company.name == branch_list[b_id], db.func.date(Orders.finalized_date) <= end,
                db.func.date(Orders.finalized_date) >= start) \
        .group_by(extract('day',Orders.finalized_date)) \
        .with_entities(func.DATE(Orders.finalized_date), db.func.sum(Orders.total).label("sum_statistics"))\
        .all()
    json = {}
    for i,row in enumerate(res):
        (day,sales) = row
        json[day] = sales
    return jsonify("{}".format(json))



if __name__== '__main__':
    app.run(debug=True,port=5000,host="127.0.0.1")
